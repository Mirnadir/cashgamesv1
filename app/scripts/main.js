$(document).ready(function() {

    // mobile nav ---[[
    // open nav
    $('.c-header-mobileMenu').click(function() {
        $('.c-mobileNav').removeClass("close-menu")
        $('.c-mobileNav').addClass("open-menu")
        $('body').css('overflow', 'hidden')
    })
    // close nav
    $('.mobile-burger').click(function() {
        $('.c-mobileNav').addClass("close-menu")
        setTimeout(function() {
            $('.c-mobileNav').removeClass("open-menu")
            $('body').css('overflow', 'auto')
        }, 400)
        
    })
    // switch to register
    $('.js-link').click(function() {
        $('.c-mobileNav').addClass("close-menu")
        setTimeout(function() {
            $('.c-mobileNav').removeClass("open-menu")
            $('.c-registrationPopup').css('display', 'block')
        }, 400)
    })
    // mobile nav ---]]

    // video on hover --- [[
    $(".c-registration__video").hover(hoverVideo, hideVideo);

    function hoverVideo(e) {  
        $('video', this).get(0).play(); 
    }

    function hideVideo(e) {
        $('video', this).get(0).pause(); 
    }
    // video on hover --- ]]

    // register popup --- [[
    $('.register').click(function(e) {
        e.preventDefault();
        $('body').css('overflow', 'hidden')
        $('.c-registrationPopup').css('display', 'block')
    })
    $('.closePopup').click(function(e) {
        $('.c-registrationPopup').css('display', 'none')
        $('body').css('overflow', 'auto')
    })  
    
    let registrationPopup = $('.c-registrationPopup').get(0)
    let alertWrapper = $('.alert-wrapper').get(0)
    let gameitemAlertWrapper = $('.gameitem-alert-wrapper').get(0)
    window.onclick = function(event) {
        if(event.target == registrationPopup) {
            $('.c-registrationPopup').css('display', 'none')
            $('body').css('overflow', 'auto')
        }

        else if(event.target == alertWrapper) {
            $('.alert-wrapper').css('display', 'none');
            $('body').css('overflow', 'auto');
        }

        else if(event.target == gameitemAlertWrapper) {
            $('.gameitem-alert-wrapper').css('display', 'none');
            $('body').css('overflow', 'auto');
        }
    }
    // register popup --- ]]

    // subscribe popup
    $('.subscribe-button').click(function(e) {
            e.preventDefault();
            var validator = $('#subscribe-form').validate()
            if(validator.form() == true) {
                $('body').css('overflow', 'hidden');
                $('.alert-wrapper').css('display', 'block');
            }
        })
    
    $('.close-button').click(function() {
        $('.alert-wrapper').css('display', 'none');
        $('body').css('overflow', 'auto');
    })
    // subscribe popup

    // gameitem popup
    $('.c-featuredGames__gameBox--item').click(function(e) {
        e.preventDefault();
        $('body').css('overflow', 'hidden');
        $('.gameitem-alert-wrapper').css('display', 'block');
    })

    $('.close-button').click(function() {
        $('.gameitem-alert-wrapper').css('display', 'none');
        $('body').css('overflow', 'auto');
    })
    // gameitem popup

    // slider in mobile --- [[
    $('.owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        dots: false,
        nav: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    // slider in mobile --- ]]
    
})
